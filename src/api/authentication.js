import axios from "axios";

export const logIn = async credentials => {
  const URL = `${process.env.REACT_APP_BASE_API_URL}/1.0/token/`;

  try {
    const { data } = await axios.post(URL, credentials);

    return data;
  } catch (error) {
    return error.response.data;
  }
};
