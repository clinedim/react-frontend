import axios from "axios";

export const registerAccount = async accountData => {
  const URL = `${process.env.REACT_APP_BASE_API_URL}/1.0/register/`;

  try {
    const { data } = await axios.post(URL, accountData);

    return data;
  } catch (error) {
    return error.response.data;
  }
};
