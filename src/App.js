import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import About from "./components/About";
import Home from "./components/Home";
import LoginForm from "./components/LoginForm";
import MoviesPage from "./pages/MoviesPage";
import Navigation from "./components/Navigation";
import NotFound from "./components/NotFound";
import RegistrationForm from "./components/RegistrationForm";
import { refreshAccessToken } from "./redux/authentication/actions";

class App extends Component {
  async componentDidMount() {
    const refreshToken = window.localStorage.getItem("refreshToken");

    if (refreshToken) {
      this.props.refreshAccessToken(refreshToken);

      console.log(refreshToken);
    }
  }

  render() {
    return (
      <BrowserRouter>
        <Navigation title="Home" />
        <Switch>
          <Route component={Home} exact path="/" />
          <Route path="/about" component={About} />
          <Route path="/login" component={LoginForm} />
          <Route path="/movies" component={MoviesPage} />
          <Route path="/register" component={RegistrationForm} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  refreshAccessToken: refreshToken => {
    dispatch(refreshAccessToken(refreshToken));
  }
});

export default connect(null, mapDispatchToProps)(App);
