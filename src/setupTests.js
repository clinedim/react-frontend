import { createMemoryHistory } from "history";
import React from "react";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";
import { createStore } from "redux";
import { render } from "@testing-library/react";

import "@testing-library/jest-dom/extend-expect";

import reducer from "./redux/root-reducer";

const initialState = {
  authentication: {
    accessToken: null,
    refreshToken: null
  },
  movies: []
};

global.renderWithRedux = function renderWithRedux(
  component,
  { initialState, store = createStore(reducer, initialState) } = {}
) {
  return {
    ...render(<Provider store={store}>{component}</Provider>)
  };
};

global.renderWithReduxAndRouter = function renderWithReduxAndRouter(
  ui,
  {
    initialState,
    store = createStore(reducer, initialState),
    route = "/",
    history = createMemoryHistory({ initialEntries: [route] })
  } = {}
) {
  return {
    ...render(
      <Provider store={store}>
        <Router history={history}>{ui}</Router>
      </Provider>
    ),
    history
  };
};

global.renderWithRouter = function renderWithRouter(
  ui,
  {
    route = "/",
    history = createMemoryHistory({ initialEntries: [route] })
  } = {}
) {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    history
  };
};
