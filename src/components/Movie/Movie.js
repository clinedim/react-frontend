import React from "react";
import PropTypes from "prop-types";

const Movie = ({ movie }) => (
  <div data-testid="movie">
    <p>
      <strong>Title:</strong>{" "}
      <span data-testid="movie-title">{movie.title}</span>
    </p>
    <p>
      <strong>Genre:</strong>{" "}
      <span data-testid="movie-genre">{movie.genre}</span>
    </p>
    <p>
      <strong>Year:</strong> <span data-testid="movie-year">{movie.year}</span>
    </p>
    <hr />
  </div>
);

Movie.propTypes = {
  movie: PropTypes.object.isRequired
};

export default Movie;
