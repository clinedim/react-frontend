import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => (
  <div>
    <h1>Not Found</h1>
    <p>The page you are looking for does not exist.</p>
    <p>
      <Link to="/">Go Home.</Link>
    </p>
  </div>
);

export default NotFound;
