import { Formik } from "formik";
import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import * as Yup from "yup";

import { logIn } from "../../redux/authentication/actions";

const LoginForm = ({ accessToken, error, logIn }) => {
  if (accessToken) {
    return <Redirect to="/" />;
  }

  return (
    <div>
      <h1>Login</h1>
      {error && <p>{error}</p>}
      <Formik
        initialValues={{
          password: "",
          username: ""
        }}
        onSubmit={async (credentials, { setSubmitting, resetForm }) => {
          await logIn(credentials);
          resetForm();
          setSubmitting(false);
        }}
        validationSchema={Yup.object().shape({
          password: Yup.string().required("Password is required."),
          username: Yup.string().required("Username is required.")
        })}
      >
        {props => {
          const {
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values
          } = props;

          return (
            <form onSubmit={handleSubmit}>
              <div>
                <label htmlFor="username-input">Username</label>
                <input
                  className={errors.username && touched.username ? "error" : ""}
                  id="username-input"
                  name="username"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  placeholder="Username"
                  type="text"
                  value={values.username}
                />
                {errors.username && touched.username && (
                  <p className="error">{errors.username}</p>
                )}
              </div>
              <div>
                <label htmlFor="password-input">Password</label>
                <input
                  className={errors.password && touched.password ? "error" : ""}
                  id="password-input"
                  name="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  placeholder="Password"
                  type="password"
                  value={values.password}
                />
                {errors.password && touched.password && (
                  <p className="error">{errors.password}</p>
                )}
              </div>
              <input disabled={isSubmitting} type="submit" value="Log In" />
            </form>
          );
        }}
      </Formik>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  logIn: credentials => {
    dispatch(logIn(credentials));
  }
});

const mapStateToProps = ({ authentication: { accessToken, error } }) => ({
  accessToken,
  error
});

LoginForm.propTypes = {
  accessToken: PropTypes.string,
  error: PropTypes.string,
  logIn: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
