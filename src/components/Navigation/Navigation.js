import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { logOut } from "../../redux/authentication/actions";

const Navigation = ({ accessToken, logOut, title }) => (
  <ul>
    <li>
      <Link to="/">{title}</Link>
    </li>
    <li>
      <Link to="/about">About</Link>
    </li>
    <li>
      <Link to="/movies">Movies</Link>
    </li>
    <li>
      <Link to="/user-status">User Status</Link>
    </li>
    <li>
      <Link to="/register">Register</Link>
    </li>
    <li>
      <Link to="/login">Log In</Link>
    </li>
    <li>
      <button onClick={() => logOut()}>Log Out</button>
    </li>
  </ul>
);

Navigation.propTypes = {
  accessToken: PropTypes.string,
  logOut: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

const mapDispatchToProps = dispatch => ({
  logOut: () => {
    dispatch(logOut());
  }
});

const mapStateToProps = ({ accessToken }) => ({ accessToken });

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
