import React from "react";
import PropTypes from "prop-types";

import Movie from "../Movie";

const MoviesList = ({ movies }) => {
  if (movies.length === 0) {
    return (
      <p data-testid="no-movies-message">
        There are no movies yet. Why not add one?
      </p>
    );
  }

  return (
    <div>
      {movies.map(movie => (
        <Movie key={movie.id} movie={movie} />
      ))}
    </div>
  );
};

MoviesList.propTypes = {
  movies: PropTypes.array.isRequired
};

export default MoviesList;
