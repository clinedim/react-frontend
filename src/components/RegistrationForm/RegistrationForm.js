import { Formik } from "formik";
import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import * as Yup from "yup";

import { registerAccount } from "../../api/registration";

const RegistrationForm = ({ accessToken }) => {
  if (accessToken) {
    return <Redirect to="/" />;
  }

  return (
    <div>
      <h1>Account Registration</h1>
      <Formik
        initialValues={{
          confirm_password: "",
          email: "",
          password: "",
          username: ""
        }}
        onSubmit={async (values, { setSubmitting, resetForm }) => {
          const result = await registerAccount(values);
          console.log(result);

          resetForm();
          setSubmitting(false);
        }}
        validationSchema={Yup.object().shape({
          confirm_password: Yup.string()
            .oneOf([Yup.ref("password"), null], "Passwords must match.")
            .required("Password confirmation is required."),
          email: Yup.string()
            .email("Enter a valid email.")
            .required("Email is required.")
            .min(6, "Email must be greater than 5 characters."),
          password: Yup.string()
            .required("Password is required.")
            .min(11, "Password must be greater than 10 characters."),
          username: Yup.string()
            .required("Username is required.")
            .min(6, "Username must be greater than 5 characters.")
        })}
      >
        {props => {
          const {
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values
          } = props;

          return (
            <form onSubmit={handleSubmit}>
              <div>
                <label htmlFor="username-input">Username</label>
                <input
                  className={errors.username && touched.username ? "error" : ""}
                  id="username-input"
                  name="username"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  placeholder="Username"
                  type="text"
                  value={values.username}
                />
                {errors.username && touched.username && (
                  <p className="error">{errors.username}</p>
                )}
              </div>
              <div>
                <label htmlFor="email-input">Email</label>
                <input
                  className={errors.email && touched.email ? "error" : ""}
                  id="email-input"
                  name="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  placeholder="Email"
                  type="email"
                  value={values.email}
                />
                {errors.email && touched.email && (
                  <p className="error">{errors.email}</p>
                )}
              </div>
              <div>
                <label htmlFor="password-input">Password</label>
                <input
                  className={errors.password && touched.password ? "error" : ""}
                  id="password-input"
                  name="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  placeholder="Password"
                  type="password"
                  value={values.password}
                />
                {errors.password && touched.password && (
                  <p className="error">{errors.password}</p>
                )}
              </div>
              <div>
                <label htmlFor="confirm-password-input">Confirm Password</label>
                <input
                  className={
                    errors.confirm_password && touched.confirm_password
                      ? "error"
                      : ""
                  }
                  id="confirm-password-input"
                  name="confirm_password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  placeholder="Confirm Password"
                  type="password"
                  value={values.confirm_password}
                />
                {errors.confirm_password && touched.confirm_password && (
                  <p className="error">{errors.confirm_password}</p>
                )}
              </div>
              <input
                disabled={isSubmitting}
                type="submit"
                value="Register Account"
              />
            </form>
          );
        }}
      </Formik>
    </div>
  );
};

const mapStateToProps = ({ authentication: { accessToken } }) => ({
  accessToken
});

RegistrationForm.propTypes = {
  accessToken: PropTypes.string
};

export default connect(mapStateToProps)(RegistrationForm);
