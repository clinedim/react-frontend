import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { createMovie, fetchMovies } from "../../redux/movies/actions";

class Movies extends Component {
  async componentDidMount() {
    await this.props.fetchMovies();
  }

  render() {
    const { createMovie, movies } = this.props;

    return (
      <div>
        <h1>Movies</h1>
        <ul>
          {movies.map(movie => (
            <li key={movie.id}>
              <Link to={`/movies/${movie.id}`}>
                {movie.title} ({movie.year}) - {movie.genre}
              </Link>
            </li>
          ))}
        </ul>
        <button
          onClick={() =>
            createMovie({ genre: "comedy", title: "Fartman!", year: "2020" })
          }
        >
          Create Movie
        </button>
        <p>
          <Link to="/contact">Click Here</Link> to contact us!
        </p>
      </div>
    );
  }
}

const mapStateToProps = ({ movies }) => ({ movies });

const mapDispatchToProps = dispatch => ({
  createMovie: ({ genre, title, year }) => {
    dispatch(createMovie({ genre, title, year }));
  },

  fetchMovies: () => {
    dispatch(fetchMovies());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Movies);
