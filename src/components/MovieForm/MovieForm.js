import { Formik } from "formik";
import React from "react";
import PropTypes from "prop-types";
import * as Yup from "yup";

import "./MovieForm.css";

const MovieForm = ({ addMovie }) => (
  <Formik
    initialValues={{
      genre: "",
      title: "",
      year: ""
    }}
    onSubmit={(values, { setSubmitting, resetForm }) => {
      addMovie(values);
      resetForm();
      setSubmitting(false);
    }}
    validationSchema={Yup.object().shape({
      genre: Yup.string()
        .required("Genre is required.")
        .min(5, "Genre must be at least 5 characters."),
      title: Yup.string()
        .required("Title is required.")
        .min(1, "Title must be at least 1 character."),
      year: Yup.number()
        .required("Year is required.")
        .moreThan(1888, "Year must be greater than 1888.")
        .lessThan(2100, "Year must be less than 2100.")
    })}
  >
    {props => {
      const {
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      } = props;

      return (
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="genre-input">Genre</label>
            <input
              className={errors.genre && touched.genre ? "input-error" : ""}
              data-testid="genre-input"
              id="genre-input"
              name="genre"
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder="Genre"
              type="text"
              value={values.genre}
            />
            {errors.genre && touched.genre && (
              <p className="error">{errors.genre}</p>
            )}
          </div>
          <div>
            <label htmlFor="title-input">Title</label>
            <input
              className={errors.title && touched.title ? "input-error" : ""}
              data-testid="title-input"
              id="title-input"
              name="title"
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder="Title"
              type="text"
              value={values.title}
            />
            {errors.title && touched.title && (
              <p className="error">{errors.title}</p>
            )}
          </div>
          <div>
            <label htmlFor="year-input">Year</label>
            <input
              className={errors.year && touched.year ? "input-error" : ""}
              data-testid="year-input"
              id="year-input"
              name="year"
              onChange={handleChange}
              placeholder="Year"
              type="text"
              value={values.year}
            />
            {errors.year && touched.year && (
              <p className="error">{errors.year}</p>
            )}
          </div>
          <input disabled={isSubmitting} type="submit" value="Create Movie" />
        </form>
      );
    }}
  </Formik>
);

MovieForm.propTypes = {
  addMovie: PropTypes.func.isRequired
};

export default MovieForm;
