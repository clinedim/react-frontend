import { CREATE_MOVIE, FETCH_MOVIES } from "./action-types";

const initialState = [];

const moviesReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_MOVIE:
      return [...state, action.movie];
    case FETCH_MOVIES:
      return [...action.movies];
    default:
      return state;
  }
};

export default moviesReducer;
