import axios from "axios";

import { FETCH_MOVIES, CREATE_MOVIE } from "./action-types";

export const createMovie = ({ genre, title, year }) => {
  return async dispatch => {
    const { data: movie } = await axios.post(
      "http://localhost:8000/api/movies/",
      {
        genre,
        title,
        year
      }
    );

    return dispatch({ type: CREATE_MOVIE, movie });
  };
};

export const fetchMovies = () => {
  return async dispatch => {
    const { data: movies } = await axios.get(
      "http://localhost:8000/api/movies/"
    );

    return dispatch({ type: FETCH_MOVIES, movies });
  };
};
