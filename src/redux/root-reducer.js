import { combineReducers } from "redux";

import authenticationReducer from "./authentication/reducer";
import moviesReducer from "./movies/reducer";

export default combineReducers({
  authentication: authenticationReducer,
  movies: moviesReducer
});
