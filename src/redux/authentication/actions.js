import axios from "axios";

import { SET_ACCESS_TOKEN, SET_ERROR } from "./action-types";

const AUTHENTICATION_URL = `${process.env.REACT_APP_BASE_API_URL}/1.0/token/`;
const REFRESH_TOKEN_URL = `${process.env.REACT_APP_BASE_API_URL}/1.0/token/refresh/`;

export const logIn = credentials => {
  return async dispatch => {
    dispatch({ type: SET_ERROR, error: null });

    try {
      const {
        data: { access: accessToken, refresh: refreshToken }
      } = await axios.post(AUTHENTICATION_URL, credentials);

      dispatch({ type: SET_ACCESS_TOKEN, accessToken });
      window.localStorage.setItem("refreshToken", refreshToken);
    } catch (error) {
      if (error.response.status === 401) {
        dispatch({
          type: SET_ERROR,
          error:
            "We were unable to log you in with the provided credentials. Please try again."
        });
      }
      console.log(error.response);
    }
  };
};

export const logOut = () => {
  window.localStorage.removeItem("refreshToken");

  return {
    type: SET_ACCESS_TOKEN,
    accessToken: ""
  };
};

export const refreshAccessToken = refreshToken => {
  return async dispatch => {
    try {
      const {
        data: { access: accessToken }
      } = await axios.post(REFRESH_TOKEN_URL, { refresh: refreshToken });

      dispatch({ type: SET_ACCESS_TOKEN, accessToken });
    } catch (error) {
      window.localStorage.removeItem("refreshToken");
    }
  };
};
