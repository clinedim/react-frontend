import { SET_ACCESS_TOKEN, SET_ERROR } from "./action-types";

const initialState = {
  accessToken: null,
  error: null
};

const authenticationReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ACCESS_TOKEN:
      return {
        ...state,
        accessToken: action.accessToken
      };
    case SET_ERROR:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
};

export default authenticationReducer;
