import React from "react";
import { cleanup, render } from "@testing-library/react";

import About from "../components/About";

afterEach(cleanup);

describe("About", () => {
  it("renders properly", () => {
    const { getByText } = render(<About />);

    expect(getByText("Add something relevant here.")).toHaveClass("content");
  });
});
