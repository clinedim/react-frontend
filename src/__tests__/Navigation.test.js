import React from "react";
import { cleanup } from "@testing-library/react";

import Navigation from "../components/Navigation";

afterEach(cleanup);

const title = "Hello, World!";

describe("Navigation", () => {
  it("matches the previous snapshot", () => {
    const { asFragment } = renderWithReduxAndRouter(
      <Navigation title={title} />
    );

    expect(asFragment()).toMatchSnapshot();
  });

  it("renders a link to the / route", () => {
    const { getByText } = renderWithReduxAndRouter(
      <Navigation title={title} />
    );

    expect(getByText(title)).toHaveAttribute("href", "/");
  });

  it("renders a link to the /about route", () => {
    const { getByText } = renderWithReduxAndRouter(
      <Navigation title={title} />
    );

    expect(getByText("About")).toHaveAttribute("href", "/about");
  });

  it("renders a link to the /movies route", () => {
    const { getByText } = renderWithReduxAndRouter(
      <Navigation title={title} />
    );

    expect(getByText("Movies")).toHaveAttribute("href", "/movies");
  });

  it("renders a link to the /user-status route", () => {
    const { getByText } = renderWithReduxAndRouter(
      <Navigation title={title} />
    );

    expect(getByText("User Status")).toHaveAttribute("href", "/user-status");
  });

  it("renders a link to the /register route", () => {
    const { getByText } = renderWithReduxAndRouter(
      <Navigation title={title} />
    );

    expect(getByText("Register")).toHaveAttribute("href", "/register");
  });

  it("renders a link to the /login route", () => {
    const { getByText } = renderWithReduxAndRouter(
      <Navigation title={title} />
    );

    expect(getByText("Log In")).toHaveAttribute("href", "/login");
  });

  it("renders a link to the /logout route", () => {
    const { getByText } = renderWithReduxAndRouter(
      <Navigation title={title} />
    );

    expect(getByText("Log Out")).toBeVisible();
  });
});
