import React from "react";
import { cleanup, render } from "@testing-library/react";

import App from "../App";

afterEach(cleanup);

it("renders", () => {
  const { asFragment } = renderWithReduxAndRouter(<App />);

  expect(asFragment()).toMatchSnapshot();
});
