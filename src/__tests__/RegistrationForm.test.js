import React from "react";
import { cleanup, render } from "@testing-library/react";

import RegistrationForm from "../components/RegistrationForm";

afterEach(cleanup);

describe("RegistrationForm", () => {
  it("matches the previous snapshot", () => {
    const { asFragment } = renderWithRedux(<RegistrationForm />);

    expect(asFragment()).toMatchSnapshot();
  });

  it("renders renders the header", () => {
    const { getByText } = renderWithRedux(<RegistrationForm />);

    expect(getByText("Account Registration")).toBeVisible();
  });

  it('renders the "Username" field', () => {
    const { getByLabelText } = renderWithRedux(<RegistrationForm />);
    const usernameInput = getByLabelText("Username");

    expect(usernameInput).toHaveAttribute("type", "text");
    expect(usernameInput).not.toHaveValue();
  });

  it('renders the "Email" field', () => {
    const { getByLabelText } = renderWithRedux(<RegistrationForm />);
    const emailInput = getByLabelText("Email");

    expect(emailInput).toHaveAttribute("type", "email");
    expect(emailInput).not.toHaveValue();
  });

  it('renders the "Password" field', () => {
    const { getByLabelText } = renderWithRedux(<RegistrationForm />);
    const passwordInput = getByLabelText("Password");

    expect(passwordInput).toHaveAttribute("type", "password");
    expect(passwordInput).not.toHaveValue();
  });

  it('renders the "Confirm Password" field', () => {
    const { getByLabelText } = renderWithRedux(<RegistrationForm />);
    const passwordInput = getByLabelText("Confirm Password");

    expect(passwordInput).toHaveAttribute("type", "password");
    expect(passwordInput).not.toHaveValue();
  });

  it('renders the "Register Account" button', () => {
    const { getByText } = renderWithRedux(<RegistrationForm />);
    const registerButton = getByText("Register Account");

    expect(registerButton).toHaveValue("Register Account");
  });
});
