import React from "react";
import { cleanup, render } from "@testing-library/react";

import Movie from "../components/Movie";

afterEach(cleanup);

const movie = {
  genre: "drama",
  title: "The Matrix",
  year: "1999"
};

it("matches the previous snapshot", () => {
  const { asFragment } = render(<Movie movie={movie} />);

  expect(asFragment()).toMatchSnapshot();
});

it("renders a title", () => {
  const { getByText } = render(<Movie movie={movie} />);

  expect(getByText(movie.title)).toBeVisible();
});

it("renders a genre", () => {
  const { getByText } = render(<Movie movie={movie} />);

  expect(getByText(movie.genre)).toBeVisible();
});

it("renders a year", () => {
  const { getByText } = render(<Movie movie={movie} />);

  expect(getByText(movie.year)).toBeVisible();
});
