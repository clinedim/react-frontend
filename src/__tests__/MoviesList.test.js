import React from "react";
import { cleanup, render } from "@testing-library/react";

import MoviesList from "../components/MoviesList";

afterEach(cleanup);

describe("MoviesList with 0 movies", () => {
  const movies = [];

  it("matches previous snapshots", () => {
    const { asFragment } = render(<MoviesList movies={movies} />);

    expect(asFragment(<MoviesList movies={movies} />)).toMatchSnapshot();
  });

  it("renders 0 movies", () => {
    const { queryAllByTestId } = render(<MoviesList movies={movies} />);
    const movieElements = queryAllByTestId("movie");

    expect(movieElements.length).toEqual(0);
  });

  it("renders a message indicating that no movies are present", () => {
    const { getByTestId } = render(<MoviesList movies={movies} />);
    const noMoviesMessage = getByTestId("no-movies-message");

    expect(noMoviesMessage).toHaveTextContent(
      "There are no movies yet. Why not add one?"
    );
  });
});

describe("MoviesList with 3 Movies", () => {
  const movies = [
    {
      id: 1,
      genre: "comedy",
      title: "Fargo",
      year: "1996"
    },
    {
      id: 2,
      genre: "thriller",
      title: "No Country for Old Men",
      year: "2007"
    },
    {
      id: 3,
      genre: "comedy",
      title: "A Serious Man",
      year: "2009"
    }
  ];

  it("matches previous snapshots", () => {
    const { asFragment } = render(<MoviesList movies={movies} />);

    expect(asFragment()).toMatchSnapshot();
  });

  it("renders 3 movies", () => {
    const { getAllByTestId } = render(<MoviesList movies={movies} />);
    const movieElements = getAllByTestId("movie");

    expect(movieElements.length).toEqual(3);
  });

  it("renders the movie titles", () => {
    const { getAllByTestId } = render(<MoviesList movies={movies} />);
    const movieTitleElements = getAllByTestId("movie-title");
    const movieTitles = movieTitleElements.map(
      movieTitleElement => movieTitleElement.textContent
    );

    expect(movieTitles).toEqual(movies.map(movie => movie.title));
  });

  it("renders the movie genres", () => {
    const { getAllByTestId } = render(<MoviesList movies={movies} />);
    const movieGenreElements = getAllByTestId("movie-genre");
    const movieGenres = movieGenreElements.map(
      movieGenreElement => movieGenreElement.textContent
    );

    expect(movieGenres).toEqual(movies.map(movie => movie.genre));
  });

  it("renders the movie years", () => {
    const { getAllByTestId } = render(<MoviesList movies={movies} />);
    const movieYearElements = getAllByTestId("movie-year");
    const movieYears = movieYearElements.map(
      movieYearElement => movieYearElement.textContent
    );

    expect(movieYears).toEqual(movies.map(movie => movie.year));
  });
});
