import React from "react";
import { cleanup, render } from "@testing-library/react";

import Home from "../components/Home";

afterEach(cleanup);

describe("Home", () => {
  it("renders", () => {
    const { getByText } = render(<Home />);

    expect(getByText("Home")).toBeVisible();
  });
});
