import React from "react";
import { cleanup, render } from "@testing-library/react";

import MovieForm from "../components/MovieForm";

afterEach(cleanup);

const defaultProps = {
  addMovie: () => true
};

describe("MovieForm", () => {
  it("matches the previous snapshot", () => {
    const { asFragment } = render(<MovieForm {...defaultProps} />);

    expect(asFragment()).toMatchSnapshot();
  });

  it('renders the "Genre" field', () => {
    const { getByLabelText } = render(<MovieForm {...defaultProps} />);
    const genreInput = getByLabelText("Genre");

    expect(genreInput).toHaveAttribute("type", "text");
    expect(genreInput).not.toHaveValue();
  });

  it('renders the "Title" field', () => {
    const { getByLabelText } = render(<MovieForm {...defaultProps} />);
    const titleInput = getByLabelText("Title");

    expect(titleInput).toHaveAttribute("type", "text");
    expect(titleInput).not.toHaveValue();
  });

  it('renders the "Year" field', () => {
    const { getByLabelText } = render(<MovieForm {...defaultProps} />);
    const yearInput = getByLabelText("Year");

    expect(yearInput).toHaveAttribute("type", "text");
    expect(yearInput).not.toHaveValue();
  });

  it('renders the "Create Movie" button', () => {
    const { getByText } = render(<MovieForm {...defaultProps} />);
    const createMovieButton = getByText("Create Movie");

    expect(createMovieButton).toHaveAttribute("type", "submit");
    expect(createMovieButton).toHaveValue("Create Movie");
  });
});
