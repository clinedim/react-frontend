import React from "react";
import { cleanup } from "@testing-library/react";

import LoginForm from "../components/LoginForm";

afterEach(cleanup);

describe("LoginForm", () => {
  it("matches the previous snapshot", () => {
    const { asFragment } = renderWithRedux(<LoginForm />);

    expect(asFragment()).toMatchSnapshot();
  });

  it("renders the header", () => {
    const { getByText } = renderWithRedux(<LoginForm />);

    expect(getByText("Login")).toBeVisible();
  });

  it('renders the "Username" field', () => {
    const { getByLabelText } = renderWithRedux(<LoginForm />);
    const usernameInput = getByLabelText("Username");

    expect(usernameInput).toHaveAttribute("type", "text");
    expect(usernameInput).not.toHaveValue();
  });

  it('renders the "Password" field', () => {
    const { getByLabelText } = renderWithRedux(<LoginForm />);
    const passwordInput = getByLabelText("Password");

    expect(passwordInput).toHaveAttribute("type", "password");
    expect(passwordInput).not.toHaveValue();
  });

  it('renders the "Log In" button', () => {
    const { getByText } = renderWithRedux(<LoginForm />);
    const loginButton = getByText("Log In");

    expect(loginButton).toHaveValue("Log In");
  });
});
