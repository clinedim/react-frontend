import axios from "axios";
import React, { Component } from "react";

import MovieForm from "../../components/MovieForm";
import MoviesList from "../../components/MoviesList";

class MoviesPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      loading: false,
      movies: []
    };
  }

  addMovie = async movieData => {
    try {
      await axios.post(
        `${process.env.REACT_APP_BASE_API_URL}/movies/`,
        movieData
      );
      await this.getMovies();
    } catch (error) {
      console.log(error);
    }
  };

  async getMovies() {
    try {
      this.setState({ loading: true });

      const { data: movies } = await axios.get(
        `${process.env.REACT_APP_BASE_API_URL}/movies/`
      );

      this.setState({ loading: false, movies });
    } catch (error) {
      console.log(error);
    }
  }

  async componentDidMount() {
    this.getMovies();
  }

  render() {
    const { addMovie } = this;
    const { loading, movies } = this.state;

    return (
      <div>
        <h1 data-testid="movies-page-header">Movies</h1>
        <MovieForm addMovie={addMovie} />
        {!loading ? <MoviesList movies={movies} /> : <p>Loading...</p>}
      </div>
    );
  }
}

export default MoviesPage;
